library("tidyverse")
library("lubridate")
library("stats")
library("lme4")
library("stargazer")
library("texreg")
library("xtable")
library("datasets")
library("ggsignif")
library("ggpubr")
library("effects")
library("broom")
library("knitr") 
library("survival")
library("cowplot")
library("blme")
library("car")
library("DHARMa")
library("glmmTMB")
library("sjPlot")
library("gridExtra")
library("performance")
library("lavaan")
library("lavaanPlot")
library("semPlot")
library("semptools")



##### Do females receive less coercion from males with whom they form stronger affiliation bonds (P1)? 

setwd("~/Nextcloud/work/10_Coercion_Sociality_Mandrills/DataOnline")
dyadiclevel <- read.csv("dyadiclevel.csv") 
# LEGEND
# Aggression: Aggression bouts from the male to the female
# Grooming_rate: Grooming rate of the dyad
# scnRate: Proximity rate of the dyad
# DS_difference: The difference of the male and female David`s scores
# operSex: Operational sex ratio
# Female_age: The age of the female in years
# Male_age: The age of the male in years
# Female: The id of the female
# Male: The id of the male
# Year: Year of observation
# dyadObsTimeAR: Time of observation of the dyad in seconds


# Grooming and proximity rate correlation test
cor.test(dyadiclevel$Grooming_rate, dyadiclevel$scnRate, method = "spearman")


# Coercion within a dyad. Main exl varialbe: grooming
modDyadSNAag <- glmmTMB(Aggression ~        # Name the model modDyadSNAagSCN to run it with scan rate instead of groom
                        + Grooming_rate
                        + DS_difference
                        + operSex
                        + Female_age
                        + poly(Male_age,2)
                        + (1 | Female) 
                        + (1 | Male)
                        + (1 | Year)
                        ,offset = log(dyadObsTimeAR)
                        ,data=dyadiclevel
                        ,family='nbinom2')

testDispersion(modDyadSNAag)
car::Anova(modDyadSNAag)
summary(modDyadSNAag)

# Figure 1a
onlyGrooming <- dyadiclevel %>% 
  select(Aggression, Female_age, Male_age, DS_difference, Grooming_rate, Female, Male, Year, operSex, dyadObsTimeAR) %>% 
  filter(complete.cases(.)) %>% 
  mutate(coeRate = fitted(modDyadSNAag)) 

  ggplot(onlyGrooming,  aes(Grooming_rate, fitted(modDyadSNAag))) +
  geom_smooth(method = "glm", method.args=list(family="nbinom1"),
              size = 1, color="black") +  # level:  Level of confidence interval to use (0.95 by default)
  geom_point(shape=16, size=1.5) +
  geom_point(aes(x=0.081, y=6.35), shape=15., size=3) +
  scale_fill_grey() +
  theme_classic() +
  xlab("Grooming rate (time of grooming \n per time of observation)") + # \n (Grooming time by time of observation)
  ylab("Predicted male coercion \n (aggression bouts)") +
  theme(panel.background = element_rect(fill = "transparent"),
        plot.background = element_rect(fill = "transparent", color = NA),
        panel.grid.major = element_blank(), # get rid of major grid
        panel.grid.minor = element_blank(), # get rid of minor grid
        legend.background = element_rect(fill = "transparent"), # get rid of legend bg
        legend.box.background = element_rect(fill = "transparent"), # get rid of legend panel bg
        axis.title = element_text(size = 21.5, color="black"),
        axis.line.y = element_line(color = "black"), 
        axis.ticks.y = element_line(color = "black"),
        axis.text.y = element_text(color = "black", size=15),
        axis.line.x = element_line(color = "black"), 
        axis.ticks.x = element_line(color = "black"),
        axis.text.x = element_text(color = "black", size=17), #, face="bold"
        axis.title.x = element_text(vjust = -0.3),
        plot.title=element_text(size=18, 
                                face="bold", 
                                family="American Typewriter",
                                color="black",
                                hjust=0.5,
                                lineheight=1.2),
        legend.text = element_text(size = 15),
        legend.title = element_text(size = 15),
        legend.justification=c(1.2,-2.5), 
        legend.position=c(0.9,0) # "none"
  )



# Coercion within a dyad. Main exl varialbe: proximty
modDyadSNAagSCN <- glmmTMB(Aggression ~        # Name the model modDyadSNAagSCN to run it with scan rate instead of groom
                               + scnRate
                             + DS_difference
                             + operSex
                             + Female_age
                             + poly(Male_age,2)
                             + (1 | Female) 
                             + (1 | Male)
                             + (1 | Year)
                             ,offset = log(dyadObsTimeAR)
                             ,data=dyadiclevel
                             ,family='nbinom2')
  
testDispersion(modDyadSNAagSCN)
car::Anova(modDyadSNAagSCN)
summary(modDyadSNAagSCN)  
  

# Figure 1b
onlyProximity <- dyadiclevel %>% 
  select(Aggression, Female_age, Male_age, DS_difference, scnRate, Female, Male, Year, operSex, dyadObsTimeAR) %>% 
  filter(complete.cases(.)) %>% 
  mutate(coeRate = fitted(modDyadSNAagSCN)) 

ggplot(onlyProximity, aes(scnRate, fitted(modDyadSNAagSCN))) + 
  geom_smooth(method = "glm", method.args=list(family="poisson"),
              size = 1, color="black") +  # level:  Level of confidence interval to use (0.95 by default)
  geom_point(shape=16, size=1.2) +
  geom_point(aes(x=0.9975, y=4.9), shape=15, size=3) +
  scale_fill_grey() +
  theme_classic() +
  xlab("Proximity rate (scans in proximity \n per total number of scans)") + # \n (Grooming time by time of observation)
  ylab(" ") +
  theme(panel.background = element_rect(fill = "transparent"),
        plot.background = element_rect(fill = "transparent", color = NA),
        panel.grid.major = element_blank(), # get rid of major grid
        panel.grid.minor = element_blank(), # get rid of minor grid
        legend.background = element_rect(fill = "transparent"), # get rid of legend bg
        legend.box.background = element_rect(fill = "transparent"), # get rid of legend panel bg
        axis.title = element_text(size = 21.5, color="black"),
        axis.line.y = element_line(color = "black"), 
        axis.ticks.y = element_line(color = "black"),
        axis.text.y = element_text(color = "black", size=15),
        axis.line.x = element_line(color = "black"), 
        axis.ticks.x = element_line(color = "black"),
        axis.text.x = element_text(color = "black", size=17), #, face="bold"
        axis.title.x = element_text(vjust = -0.3),
        plot.title=element_text(size=18, 
                                face="bold", 
                                family="American Typewriter",
                                color="black",
                                hjust=0.5,
                                lineheight=1.2),
        legend.text = element_text(size = 15),
        legend.title = element_text(size = 15),
        legend.justification=c(1.2,-2.5), 
        legend.position=c(0.9,0) # "none"
  )



##### Does a female’s top grooming/social partner protect her against coercion from other males (P2)?

setwd("~/Nextcloud/work/10_Coercion_Sociality_Mandrills/DataOnline")
topmalepartnerlevel <- read.csv("topmalepartnerlevel.csv") 
# LEGEND
# grsARnoToSsnFrnd: Aggression bouts from the all males except the top grooming male partner to the female
# frndToRateSsn: Rate of grooming of the female with her top male grooming partner
# grsARnoScnSsnFrnd: Aggression bouts from the all males except the top proximity male partner to the female
# frndScnRateSsn: Rate of proximity of the female with her top male proximity partner
# intrsxPrcntgDmdEdfv: Female rank 
# Female_age: The age of the female in years
# operSex: Operational sex ratio
# Female: The id of the female
# Year: Year of observation
# obsTimeARfem: Time of observation of the female in seconds


# Coercion from all males except the top male parner. Main exl varialbe: grooming
modSNAagFrnd <- glmmTMB(
  grsARnoToSsnFrnd ~ frndToRateSsn +
    intrsxPrcntgDmdEdfv +
    Female_age +
    operSex +
    (1 | Female) + (1 | Year),
  offset = log(obsTimeARfem),
  family = "nbinom1", # poisson nbinom2
  data = topmalepartnerlevel
  # , control-=glmmTMBControl(optCtrl = list(maxfun = 2e5)) # # "Nelder_Mead"
)

testDispersion(modSNAagFrnd) # Is the model fitted?
car::Anova(modSNAagFrnd)
summary(modSNAagFrnd)


# Figure 1c
femGrmStrngthWthFrndomplCases <- topmalepartnerlevel %>%
  select(grsARnoToSsnFrnd, frndToRateSsn,Female_age,intrsxPrcntgDmdEdfv,Female,Year,obsTimeARfem, operSex) %>%
  filter(complete.cases(.)) %>% 
  mutate(coeRate = fitted(modSNAagFrnd))


  ggplot(femGrmStrngthWthFrndomplCases, aes(frndToRateSsn, coeRate)) +
  geom_smooth(
    method = "glm", method.args=list(family="nbinom2"),
    col = "black",
    size = 1) +  # level:  Level of confidence interval to use (0.95 by default)
  geom_point(shape=16, size=1.5) +
  geom_point(aes(x=0.088, y=0.6), shape=15, size=3) +
  geom_point(aes(x=0.11, y=5.4), shape=15, size=3) +
  scale_colour_grey() +
  scale_fill_grey() +
  theme_classic() +
  xlab("Grooming with the top male partner \n (time of grooming per observation time)") +
  ylab("Predicted male coercion from \n other males (aggression bouts)") + # 
  theme(panel.background = element_rect(fill = "transparent"),
        plot.background = element_rect(fill = "transparent", color = NA),
        panel.grid.major = element_blank(), # get rid of major grid
        panel.grid.minor = element_blank(), # get rid of minor grid
        legend.background = element_rect(fill = "transparent"), # get rid of legend bg
        legend.box.background = element_rect(fill = "transparent"), # get rid of legend panel bg
        axis.title = element_text(size = 21.5, color="black"),
        axis.line.y = element_line(color = "black"), 
        axis.ticks.y = element_line(color = "black"),
        axis.text.y = element_text(color = "black", size=15),
        axis.line.x = element_line(color = "black"), 
        axis.ticks.x = element_line(color = "black"),
        axis.text.x = element_text(color = "black", size=17), #, face="bold"
        axis.title.x = element_text(vjust = -0.3, hjust = 1),
        plot.title=element_text(size=18, 
                                face="bold", 
                                family="American Typewriter",
                                color="black",
                                hjust=0.5,
                                lineheight=1.2),
        legend.position="none"
  )


# Coercion from all males except the top male parner. Main exl varialbe: proximity
modSNAagFrndSCN <- glmmTMB(
    grsARnoScnSsnFrnd ~ frndScnRateSsn +
    intrsxPrcntgDmdEdfv +
    Female_age +
    operSex +
    (1 | Female) + (1 | Year),
  offset = log(obsTimeARfem),
  family = "nbinom1", # poisson nbinom2
  data = topmalepartnerlevel
  # , control-=glmmTMBControl(optCtrl = list(maxfun = 2e5)) # # "Nelder_Mead"
)

testDispersion(modSNAagFrndSCN) # Is the model fitted?
car::Anova(modSNAagFrndSCN)
summary(modSNAagFrndSCN)




##### Does female integration in the intrasexual network or the number of maternal kin in the group protect them against male coercion (P3)?

setwd("~/Nextcloud/work/10_Coercion_Sociality_Mandrills/DataOnline")
femalenetworklevel <- read.csv("femalenetworklevel.csv") 
# LEGEND
# grsAR: Aggression bouts from the all males to the female
# grmIntraEignCntrlty: Female centrality in the female social network
# grmIntraStrengthNorm: Female strength in the female social network
# mtrlnSz: Size of the matriline of the female
# intrsxPrcntgDmdEdfv: Female rank
# Female_age: The age of the female in years
# operSex: Operational sex ratio
# Female: The id of the female
# Year: Year of observation
# obsTimeARfem: Time of observation of the female in seconds



### Is female rank correlated to SNA metrics?
cor.test(femalenetworklevel$intrsxPrcntgDmdEdfv, femalenetworklevel$grmIntraEignCntrlty, method = "spearman")
cor.test(femalenetworklevel$intrsxPrcntgDmdEdfv, femalenetworklevel$grmIntraStrengthNorm, method = "spearman")
cor.test(femalenetworklevel$intrsxPrcntgDmdEdfv, femalenetworklevel$mtrlnSz, method = "spearman")
cor.test(femalenetworklevel$intrsxPrcntgDmdEdfv, femalenetworklevel$grmIntraStrengthNorm, method = "spearman")

## Are the different SNA metrics correlated?
# Eigenvector centratily and strength
cor.test(femalenetworklevel$grmIntraEignCntrlty, femalenetworklevel$grmIntraStrengthNorm, method = "spearman")
# Eigenvector centratily and matriline size
cor.test(femalenetworklevel$grmIntraEignCntrlty, femalenetworklevel$mtrlnSz, method = "spearman")
# Eigenvector centratily and strength
cor.test(femalenetworklevel$grmIntraStrengthNorm, femalenetworklevel$mtrlnSz, method = "spearman")

## Is female age correlated to SNA metrics?
cor.test(femalenetworklevel$Female_age, femalenetworklevel$grmIntraEignCntrlty, method = "spearman")
cor.test(femalenetworklevel$Female_age, femalenetworklevel$grmIntraStrengthNorm, method = "spearman")
cor.test(femalenetworklevel$Female_age, femalenetworklevel$mtrlnSz, method = "spearman")


# Coercion from all males. Main exl varialbe: centrality
modSNAagInteg <- glmmTMB(grsAR ~
                           grmIntraEignCntrlty +
                           intrsxPrcntgDmdEdfv +
                           Female_age +
                           operSex +
                           (1 | Female) + (1 | Year),
                         offset = log(obsTimeARfem),
                         family = "nbinom2", # poisson nbinom2
                         data = femalenetworklevel
                         # , control-=glmmTMBControl(optCtrl = list(maxfun = 2e5)) # # "Nelder_Mead"
                         
)

testDispersion(modSNAagInteg) # Is the model fitted?
car::Anova(modSNAagInteg)
summary(modSNAagInteg)



# Figure 2a
indvSNAagComplCasesCentrality <- femalenetworklevel %>%
  select(grsAR,Female_age,intrsxPrcntgDmdEdfv,grmIntraEignCntrlty,Female,Year,obsTimeARfem,operSex) %>%
  filter(complete.cases(.)) %>%
  mutate(coeRate = fitted(modSNAagInteg)) 


  ggplot(indvSNAagComplCasesCentrality, aes(grmIntraEignCntrlty, coeRate)) +
  stat_smooth(method = "glm", method.args=list(family="nbinom2"),
              col = "black",
              size = 1) +  # level:  Level of confidence interval to use (0.95 by default)
  geom_point(shape=16, size=1.5) +
  scale_colour_grey() +
  scale_fill_grey() +
  theme_classic() +
  xlab("Eigenvector centrality") +
  ylab("Predicted coercion from all males \n (aggression bouts)") + # 
  theme(panel.background = element_rect(fill = "transparent"),
        plot.background = element_rect(fill = "transparent", color = NA),
        panel.grid.major = element_blank(), # get rid of major grid
        panel.grid.minor = element_blank(), # get rid of minor grid
        legend.background = element_rect(fill = "transparent"), # get rid of legend bg
        legend.box.background = element_rect(fill = "transparent"), # get rid of legend panel bg
        axis.title = element_text(size = 21.5, color="black"),
        axis.line.y = element_line(color = "black"), 
        axis.ticks.y = element_line(color = "black"),
        axis.text.y = element_text(color = "black", size=15),
        axis.line.x = element_line(color = "black"), 
        axis.ticks.x = element_line(color = "black"),
        axis.text.x = element_text(color = "black", size=17), #, face="bold"
        axis.title.x = element_text(vjust = -0.3),
        plot.title=element_text(size=18, 
                                face="bold", 
                                family="American Typewriter",
                                color="black",
                                hjust=0.5,
                                lineheight=1.2),
        legend.position="none"
  )


  
  # Coercion from all males. Main exl varialbe: strength
  modSNAagIntegSTRNGTH <- glmmTMB(grsAR ~
                             grmIntraStrengthNorm +
                             # mtrlnSz +
                             intrsxPrcntgDmdEdfv +
                             Female_age +
                             operSex +
                             (1 | Female) + (1 | Year),
                           offset = log(obsTimeARfem),
                           family = "nbinom2", # poisson nbinom2
                           data = femalenetworklevel
                           # , control-=glmmTMBControl(optCtrl = list(maxfun = 2e5)) # # "Nelder_Mead"
                           
  )
  
  testDispersion(modSNAagIntegSTRNGTH) # Is the model fitted?
  car::Anova(modSNAagIntegSTRNGTH)
  summary(modSNAagIntegSTRNGTH)
  
# Figure 2b
  indvSNAagComplCasesStrength <- femalenetworklevel %>%
  select(grsAR,Female_age,intrsxPrcntgDmdEdfv,grmIntraStrengthNorm,Female,Year,obsTimeARfem,operSex) %>%
  filter(complete.cases(.)) %>% 
  mutate(coeRate = fitted(modSNAagIntegSTRNGTH)) 


  ggplot(indvSNAagComplCasesStrength, aes(grmIntraStrengthNorm, coeRate)) +
  stat_smooth(method = "glm", method.args=list(family="nbinom2"),
              col = "black",
              size = 1) +  # level:  Level of confidence interval to use (0.95 by default)
  geom_point(shape=16, size=1.5) +
  scale_colour_grey() +
  scale_fill_grey() +
  theme_classic() +
  xlab("Strength \n (time of grooming per observation time)") +
  ylab(" ") + # 
  theme(panel.background = element_rect(fill = "transparent"),
        plot.background = element_rect(fill = "transparent", color = NA),
        panel.grid.major = element_blank(), # get rid of major grid
        panel.grid.minor = element_blank(), # get rid of minor grid
        legend.background = element_rect(fill = "transparent"), # get rid of legend bg
        legend.box.background = element_rect(fill = "transparent"), # get rid of legend panel bg
        axis.title = element_text(size = 21.5, color="black"),
        axis.line.y = element_line(color = "black"), 
        axis.ticks.y = element_line(color = "black"),
        axis.text.y = element_text(color = "black", size=15),
        axis.line.x = element_line(color = "black"), 
        axis.ticks.x = element_line(color = "black"),
        axis.text.x = element_text(color = "black", size=17), #, face="bold"
        axis.title.x = element_text(vjust = -0.3),
        plot.title=element_text(size=18, 
                                face="bold", 
                                family="American Typewriter",
                                color="black",
                                hjust=0.5,
                                lineheight=1.2),
        legend.position="none"
  )



  
  # Coercion from all males. Main exl varialbe: matriline size
  modSNAagIntegMTRLN <- glmmTMB(grsAR ~
                                    mtrlnSz +
                                    intrsxPrcntgDmdEdfv +
                                    Female_age +
                                    operSex +
                                    (1 | Female) + (1 | Year),
                                  offset = log(obsTimeARfem),
                                  family = "nbinom2", # poisson nbinom2
                                  data = femalenetworklevel
                                  # , control-=glmmTMBControl(optCtrl = list(maxfun = 2e5)) # # "Nelder_Mead"
                                  
  )
  
  testDispersion(modSNAagIntegMTRLN) # Is the model fitted?
  car::Anova(modSNAagIntegMTRLN)
  summary(modSNAagIntegMTRLN)


# Figure 2c
indvSNAagComplCasesMatriline <- femalenetworklevel %>%
  select(grsAR,Female_age,intrsxPrcntgDmdEdfv,mtrlnSz,Female,Year,obsTimeARfem,operSex) %>%
  filter(complete.cases(.)) %>% 
  mutate(coeRate = fitted(modSNAagIntegMTRLN)) 

  ggplot(indvSNAagComplCasesMatriline, aes(mtrlnSz, coeRate)) +
  stat_smooth(method = "glm", method.args=list(family="nbinom2"),  
              col = "black",
              size = 1) +  # level:  Level of confidence interval to use (0.95 by default)
  geom_point(shape=16, size=1.5) +
  scale_colour_grey() +
  scale_fill_grey() +
  theme_classic() +
  xlab("Matriline size (number of females)") +
  ylab(" ") + 
  scale_x_continuous(breaks=seq(1,10)) +
  theme(panel.background = element_rect(fill = "transparent"),
        plot.background = element_rect(fill = "transparent", color = NA),
        panel.grid.major = element_blank(), # get rid of major grid
        panel.grid.minor = element_blank(), # get rid of minor grid
        legend.background = element_rect(fill = "transparent"), # get rid of legend bg
        legend.box.background = element_rect(fill = "transparent"), # get rid of legend panel bg
        axis.title = element_text(size = 21.5, color="black"),
        axis.line.y = element_line(color = "black"), 
        axis.ticks.y = element_line(color = "black"),
        axis.text.y = element_text(color = "black", size=15),
        axis.line.x = element_line(color = "black"), 
        axis.ticks.x = element_line(color = "black"),
        axis.text.x = element_text(color = "black", size=17), #, face="bold"
        axis.title.x = element_text(vjust = -0.3),
        plot.title=element_text(size=18, 
                                face="bold", 
                                family="American Typewriter",
                                color="black",
                                hjust=0.5,
                                lineheight=1.2),
        legend.position="none"
  )





##### Is the effect of female-female bonds on sexual coercion direct, or indirectly mediated by female attractiveness or retaliation potential?

setwd("~/Nextcloud/work/10_Coercion_Sociality_Mandrills/DataOnline")
pathanalysislevel <- read.csv("pathanalysislevel.csv") 
# LEGEND
# Female_rank: Female rank
# Centrality: Female centrality in the female social network
# OSR: Operational sex ratio
# Female_age: The age of the female in years
# Reproductive_Success: Reproductive success of the female
# Coercion: Aggression rate (aggression rates divided by observation time) from all males to the female
# Female: The id of the female


# Is reproductive success correlated to female rank?
cor.test(pathanalysislevel$Reproductive_Success, pathanalysislevel$Female_rank, method = "spearman")

## Path analysis
modBondCoerPaths <- 
  '
Female_rank ~ Centrality + OSR + Female_age
Reproductive_Success ~ Centrality + Female_rank + Female_age
Coercion ~ Reproductive_Success + Female_rank + Centrality + Female_age + OSR
'

modBondCoerPaths.res <- sem(modBondCoerPaths, data=pathanalysislevel) 

summary(modBondCoerPaths.res, fit.measures=TRUE, standardized = T, rsquare = T)

pp = semPaths(modBondCoerPaths.res, 'std', layout = 'tree', 
              edge.label.cex = 1.5, sizeMan = 17, 
              sizeMan2 = 12, curvePivot = TRUE,
              label.cex = 0.75, label.prop = 1,
              nCharNodes = 0, style = "lisrel",
              fade = T, residuals = F, intercepts = F)

# get default tree layout:
tlo = pp$layout

# update layout for clearer plot:
tlo[1,1] = 10
tlo[2,1] = 10
tlo[3,1] = 15
tlo[4,1] = 0
tlo[5,1] = 0
tlo[6,1] = 0

tlo[1,2] = 8
tlo[2,2] = 0
tlo[3,2] = 4
tlo[4,2] = 4
tlo[5,2] = 9
tlo[6,2] = -1
  

# Path plot
semPaths(modBondCoerPaths.res, 'std', layout = tlo, 
         edge.label.cex = 1.5, 
         sizeMan = 17, sizeMan2 = 12,
         label.scale = FALSE,
         shapeMan="ellipse",
         nCharNodes = 0, style = "lisrel",
         fade = T, residuals = F, intercepts = F,
         posCol = c("#6699CC","white"),
         negCol = c("#882255","white"),
         nodeLabels = c("Female \n Rank", "Reproductive \n Success",
                        "Coercion", "Centrality",
                        "Operational \n Sex Ratio", "Female \n Age")
)

